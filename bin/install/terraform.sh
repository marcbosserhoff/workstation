#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh
source $(dirname $0)/repos.sh

leave_when_installed terraform

command_exists curl || ${bin}/install/curl.sh

install_terraform_linux() {
    prepare_repos
    add_hashicorp_repo
    refresh_linux_package_manager
    install_linux_package terraform
}

install_terraform_mac() {
    # Ensure we have brew installed
    command_exists brew || ${bin}/install/brew.sh

    brew tap hashicorp/tap
    brew install hashicorp/tap/terraform
}

case "$(determine_os)" in
    Linux) install_terraform_linux ;;
    Mac) install_terraform_mac ;;
    *) echo "Installation of terraform only supported under Linux/MacOs distributions";;
esac
