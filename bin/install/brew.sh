#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed brew

# Installs brew in a simple script and checks for prerequisits
# and install them when not already done

command_exists zsh || ${bin}/install/zsh.sh
command_exists curl || ${bin}/install/curl.sh
command_exists git || ${bin}/install/git.sh

# install brew

sh -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ${HOME}/.zprofile
