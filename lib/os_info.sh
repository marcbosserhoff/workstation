#!/bin/bash

SELF_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Source dependencies
source "$SELF_DIR/asserts.sh"

determine_os() {
    unameOut="$(uname -s)"
    case "${unameOut}" in
        Linux*)             echo "Linux"; exit 0;;
        Darwin*)            echo "Mac"; exit 0;;
        CYGWIN*|MINGW*)     echo "Windows"; exit 0;;
        *)                  echo "Unknown os";
    esac

    exit 1;
}

determine_user_shell() {
    grep "$(id -un)" /etc/passwd | cut -f7 -d: | rev | cut -f1 -d/ | rev
}

determine_package_manager() {
    assert_command_exists apt && echo "apt" && return 0;
    assert_command_exists yum && echo "yum" && return 0;
    assert_command_exists zypper && echo "zypper" && return 0;

    echo "No suitable package manager found"
    exit 1
}
