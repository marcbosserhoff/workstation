#!/bin/bash

LOCAL_DIR=$(dirname $0)

# Load global tool configuration
source $LOCAL_DIR/env

EXTENSION_LOCATION="$1"

if [[ -z "$EXTENSION_LOCATION" ]]; then
    error "Please provide extension location a first parameter"
    exit 1
fi

if [[ ! -f "$EXTENSION_LOCATION" ]]; then
    error "Shellrc extension script: $EXTENSION_LOCATION not found in given location" 
    exit 1
fi

cp "$EXTENSION_LOCATION" "$SHELL_RC_DIR/"
info "Added $EXTENSION_LOCATION to $SHELL_RC_DIR"
