#!/bin/bash

# Setup all linux specific stuff here for general use

LOCAL_DIR=$(dirname "$0")

# Add shellrc extension for WSL to be always in home directoy after login
"$LOCAL_DIR/../shellrc/add.sh" "$LOCAL_DIR/.shellrc.d/wsl"
