#!/bin/bash

bin=$(dirname "$0")

source "$bin/scripts/tools.sh"

# install everything needed to run your local workstation
# re-run this script to bring the workstation up-to-date
# after running a while and tools may got updates

"$bin/install/ohmyzsh.sh"
"$bin/install/ansible.sh"
"$bin/install/taskfile.sh"
"$bin/install/docker.sh"
"$bin/install/terraform.sh"
"$bin/install/packer.sh"
"$bin/install/vagrant.sh"
"$bin/install/k8s-tools.sh"
