Install WSL Ubuntu
==================

To allow the execution of powershell scripts:

Open Terminal as Administrator, run "powershell" and execute:

    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser

This allows the execution of powershell scripts locally.

If there is already a machine there from previous tests we need to clean
up first:

    wsl --unregister Ubuntu-Preview

Then we need to do very basic WSL bootstrapping to get this repo into the 
local machine to do further bootstrapping inside the machine. So we
run the following commands:

    ubuntupreview.exe install --root
    ubuntupreview.exe run git clone https://codeberg.org/marcbosserhoff/workstation.git 
    New-Item -ItemType Directory -Path $HOME/.cloud-init
    Copy-Item -Path "./workstation/bootstrap/wsl/.cloud-init/Ubuntu-Preview.user-data" -Destination "$HOME/.cloud-init/Ubuntu-Preview.user-data"

Now you have this repo cloned into the local folder on your windows machine and start bootstrapping
a new machine with cloud-init and tools:

    cd ./workstation/bootstrap/wsl

We then first cleanup the old machine and set up the new one with cloud-init:

    .\wsl_clean.ps1
    .\wsl_ubuntu.ps1

After that you can just

    wsl

into your box, being in the home folder and have the workstation project already checked out.