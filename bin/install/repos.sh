#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

command_exists curl || ${bin}/install/curl.sh

prepare_repos() {
    if [ $(linux_package_manager) != "apt" ]; then
        echo "Only apt is supported at the moment";
        exit 1
    fi

    refresh_linux_package_manager
    install_linux_package software-properties-common apt-transport-https ca-certificates gnupg lsb-release
}

add_hashicorp_repo() {
    curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
    sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
}

add_kubernetes_repo() {
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    sudo apt-add-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"
}

add_repos() {
    prepare_repos
    add_hashicorp_repo
    add_kubernetes_repo
}

# Only run when script is not sourced
if [ "$(basename $0)" = "$(basename $BASH_SOURCE)" ]; then
    case "$(determine_os)" in
        Linux) add_repos ;;
        *) echo "Installation of additional repos only supported under Linux distributions";;
    esac
fi

