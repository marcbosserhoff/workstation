#!/bin/bash

set -e

# temp: install packages, need to be rewritten on multi os tooling
sudo apt-get install jq fuse

TMP_DOWNLOAD=/tmp/jetbrains-toolbox
INSTALL_DIR="$HOME/.local/share/JetBrains/Toolbox/bin"
SYMLINK_DIR="$HOME/.local/bin"

ARCHIVE_URL=$(curl -s 'https://data.services.jetbrains.com/products/releases?code=TBA&latest=true&type=release')
URL=$(echo $ARCHIVE_URL | jq -r '.TBA[0].downloads.linux.link')

echo "Downloading Jetbrains Toolbox from $URL..."
wget -q --show-progress -cO "$TMP_DOWNLOAD" "$URL"

mkdir -p "$INSTALL_DIR"
rm "$INSTALL_DIR/jetbrains-toolbox" 2>/dev/null || true
tar -xzf "$TMP_DOWNLOAD" -C "$INSTALL_DIR" --strip-components=1
chmod +x "$INSTALL_DIR/jetbrains-toolbox"

ln -f -s "$INSTALL_DIR/jetbrains-toolbox" "$SYMLINK_DIR/jetbrains-toolbox"
