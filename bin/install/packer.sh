#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh
source $(dirname $0)/repos.sh

leave_when_installed packer

command_exists curl || ${bin}/install/curl.sh

install_packer_linux() {
    prepare_repos
    add_hashicorp_repo
    refresh_linux_package_manager
    install_linux_package packer
}

install_packer_mac() {
    # Ensure we have brew installed
    command_exists brew || ${bin}/install/brew.sh

    brew tap hashicorp/tap
    brew install hashicorp/tap/packer
}

case "$(determine_os)" in
    Linux) install_packer_linux ;;
    Mac) install_packer_mac ;;
    *) echo "Installation of packer only supported under Linux/MacOs distributions";;
esac
