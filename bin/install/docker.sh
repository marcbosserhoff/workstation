#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed docker

command_exists curl || ${bin}/install/curl.sh

add_docker_repo() {
    refresh_linux_package_manager
    install_linux_package apt-transport-https ca-certificates gnupg lsb-release

    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
}

install_docker_linux() {
    if [ $(linux_package_manager) != "apt" ]; then
        echo "Only apt is supported at the moment";
        exit 1
    fi 

    add_docker_repo
    refresh_linux_package_manager
    install_linux_package docker.io
}

install_docker_mac() {
    echo "Please visit: https://docs.docker.com/docker-for-mac/install/ and install docker desktop..."
    exit 1
}

case "$(determine_os)" in
    Linux) install_docker_linux ;;
    Mac) install_docker_mac ;;
    *) echo "Installation of docker only supported under Linux/MacOs distributions";;
esac
