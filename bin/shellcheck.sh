#!/bin/bash

FILE_OR_DIR=${1:-.}

# When parameter is a file just check that file
if [[ -f "$FILE_OR_DIR" ]]; then
    shellcheck "$FILE_OR_DIR"
    exit 0
fi

if [[ -d "$FILE_OR_DIR" ]]; then
    find "$FILE_OR_DIR" -type f -name "*.sh" -exec shellcheck {} +
    exit 0
fi

echo "First parameter is neither file nor dir, please provide a valid parameter"
exit 1
