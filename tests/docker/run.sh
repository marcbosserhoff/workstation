#!/bin/bash

set -e -uo pipefail

# start a test container with nginx frontend on port 80

echo "Starting docker container for test..."
docker run -d -p 80:80 --rm --name docker_test docker/getting-started

# check if curl will be successful
echo "Running curl on port 80 for the container..."
curl http://localhost > /dev/null

# cleanup and exit script
echo "Stopping the container..."
docker stop docker_test

echo "Docker test was successfull"
