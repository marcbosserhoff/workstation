# Workstation

Setup and tools for your development environment/workstation. To install your computer to be able
to work with cloud stuff just call:

    ./bin/init.sh

If you want to know, that everything is ok, there is another script that checks, if everything
is runnable:

    ./bin/check.sh

# Fixing local python setups in user space

According to your Python version (for example 3.8) you must add the user space installation to your path
either in .bashrc or .zshrc at the end and restart your terminal:

    export PATH="$PATH:$HOME/Library/Python/3.8/bin"

