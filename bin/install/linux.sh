#!/bin/bash

set -e
sudo apt-get update
sudo apt-get -y install jq fuse shellcheck shunit2 fzf
sudo apt-get -y upgrade
sudo apt-get -y autoremove
