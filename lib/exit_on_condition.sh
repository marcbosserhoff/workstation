#!/bin/bash

SELF_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Source dependencies
source "$SELF_DIR/asserts.sh"

exit_when_not_installed() {
    if ! assert_command_exists "$1"; then
        echo "'$1' is not installed!" >&2
        exit 1
    fi
}

exit_when_installed() {
    if assert_command_exists "$1"; then
        echo "'$1' is already installed!" >&2
        exit 1
    fi
}

exit_when_folder_exists() {
    if [ -d "$1" ]; then
        echo "'$1' folder already exists!" >&2
        exit 1
    fi
}
