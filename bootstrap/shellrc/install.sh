#!/bin/bash

LOCAL_DIR=$(dirname "$0")

# Load global tool configuration
source "$LOCAL_DIR"/env

# Obtain user shell to extend the right configuation files to run shellrc
USER_SHELL_TYPE="$(cat /etc/passwd | grep $(id -un) | cut -f7 -d: | rev | cut -f1 -d/ | rev)"
USER_SHELL_RC_FILE="$HOME/.${USER_SHELL_TYPE}rc"

info "Installing extension with configuration: "
info "SHELL_RC_DIR: $SHELL_RC_DIR"
info "SHELL_RC_SCRIPT_LOCATION: $SHELL_RC_SCRIPT_LOCATION"
info "USER_SHELL_TYPE: $USER_SHELL_TYPE"
info "USER_SHELL_RC_FILE: $USER_SHELL_RC_FILE"

# Check if the directory exists
if [[ ! -d "$SHELL_RC_DIR" ]]; then
    mkdir -p "$SHELL_RC_DIR" || { error "Failed to create directory: %s\n" "$SHELL_RC_DIR"; return 1; }
    info "Created directory for shellrc extension $SHELL_RC_DIR"
else
    info "$SHELL_RC_DIR already exists."
fi

# Write seed script to specified location
cp "$LOCAL_DIR/$SHELL_RC_SCRIPT" "$SHELL_RC_SCRIPT_LOCATION"
chmod +x "$SHELL_RC_SCRIPT_LOCATION"
info "Installed seed script to $SHELL_RC_SCRIPT_LOCATION"

# Check if seed script is already sources in user shell rc file
if ! grep -qF -- "$SHELL_RC_SOURCING_STRING" "$USER_SHELL_RC_FILE"; then
    echo "$SHELL_RC_SOURCING_STRING" >> "$USER_SHELL_RC_FILE"
    info "Seed script was added in $USER_SHELL_RC_FILE. Please restart shell to become active."
else
    info "Seed script is already configured in $USER_SHELL_RC_FILE"
fi
