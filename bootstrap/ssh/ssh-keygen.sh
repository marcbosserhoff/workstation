#!/bin/bash

LOCAL_DIR=$(dirname $0)

SSH_KEY_NAME="${1:-id}"

source $LOCAL_DIR/env

# Only create ssh key, if key is not already there
if [[ ! -f "$SSH_KEY_PATH" ]]; then
	# Create the directory if it doesn't exist
	mkdir -p "$(dirname $SSH_KEY_PATH)"

	# Generate the SSH key without a passphrase
	ssh-keygen -t $SSH_KEY_TYPE -b $SSH_KEY_LENGTH -f $SSH_KEY_PATH -N ""

	# Set proper permissions
	chmod 600 "${SSH_KEY_PATH}"
	chmod 644 "${SSH_PUBKEY_PATH}"
else
	info "SSH key already exists in location ${SSH_KEY_PATH}"
fi

echo "Printing Public Key ${SSH_PUBKEY_PATH} to copy for your git setup..."
cat "${SSH_PUBKEY_PATH}"
