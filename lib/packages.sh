#!/bin/bash

SELF_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

refresh_linux_packages() {
    pm="$(determine_package_manager)"
    update="${1:-0}"
    upgrade="${2:-0}"

    if [[ "$update" == "1" ]]; then
        sudo "$pm" update
    fi
    if [[ "$upgrade" == "1" ]]; then
        sudo "$pm" upgrade
    fi
}

install_linux_package() {
    sudo "$(linux_package_manager)" install -y "$@"
}

install_or_upgrade_pip3_package() {
    python3 -m pip install --upgrade --user "$@"
}

