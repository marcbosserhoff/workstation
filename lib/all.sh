#!/bin/bash

# Wrapper for all shell tools to avoid dependency management

# Usage:
#
#     source all.sh
#
# or
#
#     . all.sh
#
# TESTING: If you want to test this file locally in your shell please
# just run this file and its automatically creating a subshell
# so that, for example you are in WSL an error will not completely
# leave your wsl instance

SELF_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

source "$SELF_DIR/asserts.sh"
source "$SELF_DIR/exit_on_condition.sh"
source "$SELF_DIR/os_info.sh"
source "$SELF_DIR/packages.sh"

# Set the failure modes only when the file is sourced,
# so it is applied for the installation scripts and does
# not disturb local testing and development
if [[ "${BASH_SOURCE[0]}" != "$0" ]]; then
    set +u -eo pipefail
else
    # Check if file is not sourced, otherwise start a subshell
    # to be able to test this script
    echo "TESTING mode active. Creating subshell..."
    bash --rcfile "$0" -i
fi
