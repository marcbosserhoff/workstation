#!/bin/bash

# Include general test setup preparations
# common for all tests in this directory
. "$(dirname "$0")/prepare_tests.sh"

# Include the script containing the function to be tested
. "$(dirname "$0")/lib/exit_on_condition.sh"

test_exit_when_not_installed_on_foobar() {
    foobar="$(exit_when_not_installed foobar)"
    foobar_exit_code=$?

    assertEquals  "Method should exit as 'foobar' is not installed" "1" "$foobar_exit_code"
}

test_exit_when_not_installed_on_ls() {
    ls="$(exit_when_not_installed ls)"
    ls_exit_code=$?

    assertEquals  "Method should not exit as 'ls' is installed" "0" "$ls_exit_code"
}

test_exit_when_installed_on_foobar() {
    foobar="$(exit_when_installed foobar)"
    foobar_exit_code=$?

    assertEquals  "Method should not exit as 'foobar' is not installed" "0" "$foobar_exit_code"
}

test_exit_when_installed_on_ls() {
    ls="$(exit_when_installed ls)"
    ls_exit_code=$?

    assertEquals  "Method should exit as 'ls' is installed" "1" "$ls_exit_code"
}

test_exit_when_folder_exists() {
    folder="$(exit_when_folder_exists /etc)"
    folder_exit_code=$?

    assertEquals  "Method exit as '/etc' is existing everywhere in Linux" "1" "$folder_exit_code"
}

# Import the shunit2 library
. shunit2
