#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh
source $(dirname $0)/repos.sh

leave_when_installed vagrant

command_exists curl || ${bin}/install/curl.sh

install_vagrant_linux() {
    prepare_repos
    add_hashicorp_repo
    refresh_linux_package_manager
    install_linux_package vagrant
}

install_vagrant_mac() {
    # Ensure we have brew installed
    command_exists brew || ${bin}/install/brew.sh

    brew tap hashicorp/tap
    brew install hashicorp/tap/vagrant
}

case "$(determine_os)" in
    Linux) install_vagrant_linux ;;
    Mac) install_vagrant_mac ;;
    *) echo "Installation of vagrant only supported under Linux/MacOs distributions";;
esac
