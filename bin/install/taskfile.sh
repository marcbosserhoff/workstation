#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed task

install_task_via_curl_sh() {
    # Ensure we have curl
    command_exists curl || ${bin}/install/curl.sh

    sudo sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin
}

install_task_mac() {
    # Ensure we have brew installed
    command_exists brew || ${bin}/install/brew.sh

    brew install go-task/tap/go-task
}

case "$(determine_os)" in
    Mac) install_task_mac ;;
    *) install_task_via_curl_sh ;;
esac
