#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed ansible

# Installs ansible with prerequisites
# and install them when not already done

command_exists pip3 || ${bin}/install/pip3.sh

# install ansible and paramiko

install_or_upgrade_pip3_package ansible
install_or_upgrade_pip3_package paramiko
