#!/bin/bash

bin=$(dirname "$0")

source "$bin/tools.sh"

docker build --rm -t workstation .

IMAGE_ID=$(docker images -q workstation)
docker run -it "$IMAGE_ID" /bin/bash

