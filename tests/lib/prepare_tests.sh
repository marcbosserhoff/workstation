#!/bin/bash

info() {
  echo "[TEST][$(basename "$0")] >>> $@"
}

error() {
  echo "[TEST][$(basename "$0")] >>> $@" >&2
}

oneTimeSetUp() {
  info "Running tests:"
}

oneTimeTearDown() {
  info "Tests finished."
}

