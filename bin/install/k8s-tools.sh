#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh
source $(dirname $0)/repos.sh

leave_when_installed kubectl

command_exists curl || ${bin}/install/curl.sh

install_kubectl_linux() {
    prepare_repos
    add_kubernetes_repo
    refresh_linux_package_manager
    install_linux_package kubectl
}

install_kubectl_mac() {
    # Ensure we have brew installed
    command_exists brew || ${bin}/install/brew.sh

    brew install kubernetes-cli
}

case "$(determine_os)" in
    Linux) install_kubectl_linux ;;
    Mac) install_kubectl_mac ;;
    *) echo "Installation of kubectl only supported under Linux/MacOs distributions";;
esac
