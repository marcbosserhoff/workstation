#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed zsh

install_zsh_linux() {
    refresh_linux_package_manager
    install_linux_package zsh
}

case "$(determine_os)" in
    Linux) install_zsh_linux ;;
    *) echo "Installation of zsh only supported under Linux distributions";;
esac
