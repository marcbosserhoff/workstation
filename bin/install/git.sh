#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed git

install_git_linux() {
    refresh_linux_package_manager
    install_linux_package git-all
}

install_git_mac() {
    xcode-select --install
}

case "$(determine_os)" in
    Linux) install_git_linux ;;
    Mac) install_git_mac ;;
    *) echo "Installation of git only supported under Linux/MacOs distributions";;
esac
