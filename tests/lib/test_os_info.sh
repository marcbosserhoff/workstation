#!/bin/bash

# Include general test setup preparations
# common for all tests in this directory
. "$(dirname "$0")/prepare_tests.sh"

# Include the script containing the function to be tested
. "$(dirname "$0")/lib/os_info.sh"

test_determine_os() {
    os=$(determine_os)
    assertEquals "Detected Os should be linux" "Linux" "$os"
}

test_determine_user_shell() {
    user_shell=$(determine_user_shell)
    assertEquals "Detected shell should be bash" "bash" "$user_shell"
}

test_determine_package_manager() {
    pm=$(determine_package_manager)
    assertEquals "Detected package manager should be apt" "apt" "$pm"
}

# Import the shunit2 library
. shunit2
