#!/bin/bash

LOCAL_DIR=$(dirname "$0")

# Add shellrc extension for automatically knowing of the local tool paths
"$LOCAL_DIR/../shellrc/add.sh" "$LOCAL_DIR/.shellrc.d/local-path"
