#!/bin/bash

SELF_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

github_url () {
  tool=$1
  version=${2:-latest}


}

jetbrains_url() {
  TOOL_CODE=$1
  VERSION=${2:-latest}

  SEARCH_APPEND=""
  if [[ "$VERSION" == "latest" ]]; then
    SEARCH_APPEND="&latest=true"
  else
    SEARCH_APPEND="&version=$VERSION"
  fi

  SEARCH_URL="https://data.services.jetbrains.com/products/releases?code=$TOOL_CODE&type=release$SEARCH_APPEND"
  echo $SEARCH_URL

  SEARCH_RESULT=$(curl -s $SEARCH_URL)
  #echo $SEARCH_RESULT
  echo $SEARCH_RESULT | jq -r '.[][0].downloads.linux.link'
}

jetbrains_product_codes() {
cat <<EOF
List of jetbrains product codes usable for 'jetbrains_url':

IU    IntelliJ IDEA Ultimate
IC    IntelliJ IDEA Community
IE    IntelliJ IDEA Educational
PS    PhpStorm
WS    WebStorm
PY    PyCharm Professional
PC    PyCharm Community
PE    PyCharm Educational
RM    RubyMine
OC    AppCode
CL    CLion
GO    GoLand
DB    DataGrip
RD    Rider
AI    Android Studio
EOF
}