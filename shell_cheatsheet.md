# Print multiline content

    cat <<EOF

    ... TEXT HERE ...
        
    EOF

# Check if no params are given/non-tty maybe

    if [ ! -t 0 ]; then
        NON_TTY=yes
    fi

# Parse arguments in a loop

    while [ $# -gt 0 ]; do
        case $1 in
            --go) GO=yes; JAVA=no ;;
            --java) JAVA=YES ;;
        esac
        shift
    done

# Check for command

    # Will check for command and return exit code
    command_exists() {
        command -v "$@" >/dev/null 2>&1
    }

    # Inline command check
    command_exists git || {
        echo "git is not installed"
        exit 1
    }

    # If-Check

    if ! command_exists git; then
        ...
    fi

# Read line and parse input from var (could also any variable)

    read -r opt
    case $opt in
        y*|Y*|"") echo "Yes..." ;;
        n*|N*) echo "No."; return ;;
        *) echo "Invalid choice. Skipping..."; return ;;
    esac

# Runtime vars

    $? - Last exit status
    $# - Parameter count in current scope (script, method, etc)
    $1, $2, .. - Concrete parameter
    $0 - Script/Method name
    $@ - All parameters given (except 'shift'ed vars)
    $$ - PID of current running shell
