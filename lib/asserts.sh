#!/bin/bash

SELF_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

assert_command_exists() {
	command -v "$@" >/dev/null 2>&1
}

