#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed pip3

command_exists curl || ${bin}/install/curl.sh

install_pip3() {
    curl https://bootstrap.pypa.io/get-pip.py | python3 - --user

    current_shell=$(determine_user_shell)
    echo "Attaching PATH extension for Python to your .${current_shell}rc..."
    echo 'export PATH="$(python3 -m site --user-base)/bin:$PATH"' >> ${HOME}/.${current_shell}rc
}

case "$(determine_os)" in
    Linux|Mac) install_pip3 ;;
    *) echo "Installation of pip only supported under Linux/MacOs distributions";;
esac
