#!/bin/bash

# Function to run scripts that start with 'test_'
run_all_tests() {

  # Find and execute each script starting with 'test_'
  local script
  for script in test_*; do
    if [[ -x "$script" ]]; then
      echo "[TEST][$script] >>> Starting script..."
      ./"$script" || echo "[TEST][$script] Error: Script failed to execute properly." >&2
    else
      echo "[TEST][$script] Skipping non-executable file" >&2
    fi
  done
}

# Start all tests in this directory
run_all_tests "$(dirname "$0")"
