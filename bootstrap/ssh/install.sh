#!/bin/bash

LOCAL_DIR=$(dirname $0)

# Create (if not already existing) a default ssh-key id.rsa and id.rsa.pub
$LOCAL_DIR/ssh-keygen.sh

# Add shellrc extension for automatically loading all ssh keys at startup
$LOCAL_DIR/../shellrc/add.sh $LOCAL_DIR/.shellrc.d/ssh-add-keys
