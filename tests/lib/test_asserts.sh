#!/bin/bash

# Include general test setup preparations
# common for all tests in this directory
. "$(dirname "$0")/prepare_tests.sh"

# Include the script containing the function to be tested
. "$(dirname "$0")/lib/asserts.sh"

# Test case to check if a command exists
test_assert_command_exists() {
    assertTrue "Command 'ls' should exist" "assert_command_exists ls"
    assertFalse "Command 'foobar' should not exist" "assert_command_exists foobar"
}

# Import the shunit2 library
. shunit2
