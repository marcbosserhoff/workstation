#!/bin/bash

bin=$(dirname $0)/..

source ${bin}/tools.sh

leave_when_installed curl

install_curl_linux() {
    refresh_linux_package_manager
    install_linux_package curl
}

case "$(determine_os)" in
    Linux) install_curl_linux ;;
    *) echo "Installation of curl only supported under Linux distributions";;
esac
