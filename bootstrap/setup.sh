#!/bin/bash

# Setup all specific stuff here for general use

LOCAL_DIR=$(dirname $0)

# First install shellrc as everything else depends on it
# To be able to put user profiles easily without
# modifying .bashrc or .zshrc anymore
$LOCAL_DIR/shellrc/install.sh


# Install all base linux spec stuff like some base packages
# like jq and also make the $HOME/.local paths available
# to store personal tools without sudo
$LOCAL_DIR/linux/install.sh

# Basic setup to have general linux stuff running
# like ssh keys setup to be able to use git or additions to wsl

$LOCAL_DIR/ssh/install.sh
